<!DOCTYPE html>
<html>
<head>
	<title>Word cloud</title>
</head>
<body>
	<div class="container">
		<h4>twitter analyst</h4>
		<div id="wordCloud"></div>
	</div>

	<script src="d3.min.js"></script>
	<script src="d3.layout.cloud.js"></script>

<script>

    var frequency_list = [
    <?php
    	for($i = 1; $i <= 50; $i++) {
    		echo "{'text': '".uniqid()."', 'size': ".rand(1, rand(1, 10))."}, " . PHP_EOL;
    	}
    	echo "{'text': '".uniqid()."', 'size': ".rand(1, 10)."}";
    ?>
	];


    var fill = d3.scale.category20();

    d3.layout.cloud().size([800, 300])
        .words(frequency_list)
        .rotate(0)
        .fontSize(function(d) { return d.size; })
        .on("end", draw)
        .start();

    function draw(words) {
        d3.select("body").append("svg")
                .attr("width", 850)
                .attr("height", 350)
                .attr("class", "wordcloud")
                .append("g")
                // without the transform, words words would get cutoff to the left and top, they would
                // appear outside of the SVG area
                .attr("transform", "translate(320,200)")
                .selectAll("text")
                .data(words)
                .enter().append("text")
                .style("font-size", function(d) { return d.size*2 + 12 + "px"; })
                .style("fill", function(d, i) { return fill(i); })
                .attr("transform", function(d) {
                    return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
                })
                .text(function(d) { return d.text; });
    }
</script>
</body>
</html>